package com.marcolicci;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.time.LocalDate;

public class Controller {
    @FXML
    private TextField areaField;
    @FXML
    private TextField numberField;
    @FXML
    private DatePicker arrivalDatePicker;
    @FXML
    private TextField stayDaysField;
    @FXML
    private ToggleGroup contractTypeToggleGruop;
    @FXML
    private RadioButton summerContractTypeRadio;
    @FXML
    private RadioButton annulContractTypeRadio;
    @FXML
    private RadioButton customContractTypeRadio;
    @FXML
    private CheckBox availableCheck;
    @FXML
    private CheckBox parkingCheck;
    @FXML
    private Button updateButton;
    @FXML
    private Button checkoutButton;
    @FXML
    private GridPane areaGrid0;
    @FXML
    private GridPane areaGrid1;
    @FXML
    private GridPane areaGrid2;
    @FXML
    private GridPane areaGrid3;
    @FXML
    private TextArea display;

    private GridPane[] areaGrids;


    public Controller() {
        areaGrids = new GridPane[4];
    }

    public void initialize(){
        areaGrids[0] = areaGrid0;
        areaGrids[1] = areaGrid1;
        areaGrids[2] = areaGrid2;
        areaGrids[3] = areaGrid3;
        Button campsite;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 2; k++) {
                    int area = i;
                    int number = 2 * j + k;
                    campsite = new Button(Integer.toString(number + 1));
                    campsite.setOnAction((event)->{
                        render(area, number);
                    });
                    areaGrids[i].add(campsite, j, k);
                    if(j == 2 && k == 0)
                        k++;
                }
            }
        }
        contractTypeToggleGruop.selectToggle(summerContractTypeRadio);
    }

    public void render(int area, int number){
        Campsite campsite;
        areaField.setText(Integer.toString(area + 1));
        numberField.setText(Integer.toString(number + 1));
        try{
            campsite = Main.getCamping().getCampsite(area, number);
        } catch (Exception e){
            Main.getCamping().setCampsite(area, number, new Campsite(area, number, true, null, 0, null));
            campsite = Main.getCamping().getCampsite(area, number);
        }

        availableCheck.setSelected(campsite.isAvailable());
        if(campsite.getArrivalDay() != null){
            arrivalDatePicker.setValue(campsite.getArrivalDay());
        }
        else {
            arrivalDatePicker.setValue(null);
        }
        if(campsite.getStayDays() > 0){
            stayDaysField.setText(Integer.toString(campsite.getStayDays()));
        }
        else{
            stayDaysField.setText("");
        }
        if(campsite.getContractType() != null){
            switch (campsite.getContractType()){
                case SUMMER:
                    contractTypeToggleGruop.selectToggle(summerContractTypeRadio);
                    break;
                case ANNUAL:
                    contractTypeToggleGruop.selectToggle(annulContractTypeRadio);
                    break;
                case CUSTOM:
                    contractTypeToggleGruop.selectToggle(customContractTypeRadio);
                    break;
            }
        }
        parkingCheck.setSelected(campsite instanceof CampsiteWithParking);
    }

    public void update(){
        int area = Integer.parseInt(areaField.getText());
        int number = Integer.parseInt(numberField.getText());
        if(area > 0 && area < 5 && number > 0 && number < 6){
            area--;
            number--;
            LocalDate date;
            try{
                date = arrivalDatePicker.getValue();
            }catch (Exception e){
                date = null;
            }
            int stayDays;
            try {
                stayDays = Integer.parseInt(stayDaysField.getText());
            } catch (Exception e) {
                stayDays = -1;
            }
            Main.getCamping().setCampsite(area, number,
                    new Campsite(area, number, availableCheck.isSelected(), date, stayDays,
                            Campsite.ContractType.valueOf(((RadioButton) contractTypeToggleGruop.getSelectedToggle())
                                    .getText().toUpperCase())));
        }
    }

    public void onLocationFieldsEdit(ActionEvent event){
        int area = Integer.parseInt(areaField.getText());
        int number = Integer.parseInt(numberField.getText());
        if(area > 0 && area < 5 && number > 0 && number < 6){
            render(area - 1, number - 1);
        }
    }

    public void printInvoice(){
        display.setText(Main.getCamping().invoice(
                Integer.parseInt(areaField.getText()) - 1, Integer.parseInt(areaField.getText()) - 1));
    }
}




