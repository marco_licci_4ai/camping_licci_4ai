package com.marcolicci;

import java.time.LocalDate;

/**
 * Created by marco on 25/02/2017.
 */
public class CampsiteWithParking extends Campsite {
    public CampsiteWithParking(int area, int number, boolean available, LocalDate stayFirstDay, int stayDays, ContractType contractType) {
        super(area, number, available, stayFirstDay, stayDays, contractType);
    }

    @Override
    public int amountDue(){
        int amount = super.amountDue();
        if(getContractType() == ContractType.CUSTOM){
            return amount + (int) Math.ceil((float) amount * 0.2);
        }
        else{
            return amount;
        }
    }
}
