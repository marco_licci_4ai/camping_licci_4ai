package com.marcolicci;

import java.util.ArrayList;

/**
 * Created by marco on 25/02/2017.
 */
public class Camping {
    private ArrayList<Campsite>[] areas;

    public Camping() {
        areas = new ArrayList[4];
        for (int i = 0; i < 4; i++) {
            areas[i] = new ArrayList(5);
        }
        //areas[0].add(new Campsite(0, 0, true, null, 5, Campsite.ContractType.ANNUAL));
        //System.out.println(areas[0].get(0) instanceof CampsiteWithParking);
    }

    public Campsite getCampsite(int area, int number){
        return areas[area].get(number);
    }

    public void setCampsite(int area, int number, Campsite campsite){
        areas[area].add(number, campsite);
    }

    public String invoice(int area, int number){
        Campsite campsite = getCampsite(area, number);
        String invoice = "";
        invoice += "Amount due: " + campsite.amountDue();
        invoice += "\nContract type: " + campsite.getContractType().toString().toLowerCase();
        invoice += "\nParking included: " + (campsite instanceof CampsiteWithParking ||
                !(campsite.getContractType() == Campsite.ContractType.CUSTOM) ? "yes" : "no");
        return invoice;
    }
}
