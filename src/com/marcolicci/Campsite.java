package com.marcolicci;

import java.time.LocalDate;

/**
 * Created by marco on 25/02/2017.
 */
public class Campsite {
    protected enum ContractType { ANNUAL, SUMMER, CUSTOM }
    private int area;
    private int number;
    private boolean available;
    private LocalDate arrivalDay;
    private int stayDays;
    private ContractType contractType;

    public Campsite(int area, int number, boolean available, LocalDate arrivalDay, int stayDays, ContractType contractType) {
        this.area = area;
        this.number = number;
        this.available = available;
        this.arrivalDay = arrivalDay;
        this.stayDays = stayDays;
        this.contractType = contractType;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public LocalDate getArrivalDay() {
        return arrivalDay;
    }

    public void setArrivalDay(LocalDate arrivalDay) {
        this.arrivalDay = arrivalDay;
    }

    public int getStayDays() {
        return stayDays;
    }

    public void setStayDays(int stayDays) {
        this.stayDays = stayDays;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public int amountDue(){
        switch (contractType){
            case ANNUAL:
                return 1500;
            case SUMMER:
                return 9000;
            case CUSTOM:
                if (stayDays <= 3)
                    return stayDays * 12;
                else if (stayDays <= 15)
                    return stayDays * 10;
                else
                    return stayDays * 8;
        }
        return -1;
    }

}